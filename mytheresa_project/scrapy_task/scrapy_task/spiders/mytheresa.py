import scrapy
from ..items import Product
from scrapy_redis.spiders import RedisSpider


class MytheresaSpider(RedisSpider):
    name = "mytheresa"
    # start_urls = ['https://www.mytheresa.com/en-de/shoes.html',
    #               'https://www.mytheresa.com/en-de/clothing/dresses.html']

    def parse(self, response):
        products_links = response.xpath(
            '//ul[contains(@class, "products-grid products-grid--max-3-col")]/li[@class="item"]/a/@href'
        ).extract()

        for link in products_links:
            yield scrapy.Request(url=link,
                                 callback=self.parse_product)

    def parse_product(self,response):
        product = Product()

        product['item_number'] = response.xpath(
            '//div[@class="product-sku"]/span/text()'
        ).extract()

        product['name'] = response.xpath(
            '//div[@class="product-name"]/span/text()'
        ).extract()

        product['images'] = response.xpath(
            '//div[contains(@class, "product-image-gallery")]//img[@id]/@src'
        ).extract()

        product['price'] = response.xpath(
            '//div[@class="product-shop"]/div[@class="price-info"]/div[@class="price-box"]/span/span/text()'
        ).extract()

        product['details'] = response.xpath(
            '//p[contains(@class, "pa1 product-description")]/text()'
        ).extract()

        product['sizes'] = response.xpath(
            '//ul[@class="sizes"]/li/a[@class="addtocart-trigger"]/text()'
        ).extract()

        product['size_description'] = response.xpath(
            '//div[@class="fit-advisor"]/ul[@class="disc featurepoints"]/li/text()'
        ).extract()

        product['brand'] = response.xpath(
            '//div[@class="product-shop"]/div[@class="product-designer"]/span/a/text()'
        ).extract()

        product['url'] = response.url

        yield product
