# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Product(scrapy.Item):
    item_number = scrapy.Field()
    name = scrapy.Field()
    images = scrapy.Field()
    price = scrapy.Field()
    details = scrapy.Field()
    sizes = scrapy.Field()
    size_description = scrapy.Field()
    brand = scrapy.Field()
    url = scrapy.Field()