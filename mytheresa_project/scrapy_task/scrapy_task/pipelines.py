# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import sys
import os
import django
from scrapy.utils.serialize import ScrapyJSONEncoder


encoder = ScrapyJSONEncoder()

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(
    os.path.abspath(__file__))), 'mytheresa_project'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'mytheresa_project.settings'
django.setup()

from products.tasks import new_item


class MytheresaProductPipeline(object):
    def process_item(self, item, spider):
        new_item.delay(encoder.encode(item))
        return item



