from django.db import models

# Create your models here.


class Product(models.Model):
    item_number = models.TextField()
    name = models.TextField()
    images = models.TextField()
    price = models.TextField()
    details = models.TextField()
    sizes = models.TextField()
    size_description = models.TextField()
    brand = models.TextField()
    url = models.URLField()

    def __str__(self):
        return str(self.id) + '. ' + self.name