from .models import Product
from mytheresa_project.celery import app
import json


@app.task
def new_item(item):
    item = json.loads(item)
    Product.objects.get_or_create(
        name=item['name'],
        brand=item['brand'],
        price=''.join(item['price']),
        details=''.join(item['details']),
        sizes=', '.join(item['sizes']),
        size_description=''.join(item['size_description']),
        item_number=''.join(item['item_number']),
        images=',\n'.join(item['images']),
        url=',\n'.join(item['url'])
    )

