from django.shortcuts import render,redirect
from django.conf import settings
from django.views import View
from django.views.generic import ListView
from products.models import Product
import redis





class IndexView(View):
    def post(self,request):
        if request.method == 'POST':
            redis_server = redis.Redis(settings.REDIS_HOST, settings.REDIS_PORT)
            redis_server.lpush('mytheresa:start_urls','https://www.mytheresa.com/en-de/clothing/dresses.html')
            return redirect('parsed_products')

    def get(self, request):
        return render(request,'products/index.html')


class ParsedProductsListView(ListView):
    model = Product
    template_name = 'products/parsed_data.html'


